from pyramid.view import view_config
import random
import hashlib
import pyqrcode
from datetime import datetime, timedelta
import io

mode = False
#mode = "demo"

projekte= [

    '298ef47280aa69c16c0442caff473be8ec5934f4',  
    '9a6805e9b529890e00d86779d2f686d24daa120e',
    '353da334523a5965a19ba6dd8e78a33354a423ec',
    '3b3d7f83e0d042ad125aff553afd584d046e3378',
    'bfce5860e32bcab13e42e80e3fad65f0103ccfc5',
    '7d86d8736551c87650f0a57c822cce0178a287b5',
    'a6caf962d20be788642d2dc78f438da1f38acd21',
    '9c52c97b9730e6a6ebae3b4b5f5bca18311a385a',
    '81be2badb7d9b7260593d4d367ddb17affe1242e',
    '3bb6ec421f18f692f01ffab1e08f9df8b2af98e3',
    '9e2f9223f35fe45165daaa3cd1fb97ca8ceeab23',
    '952c3b73214ad2a060afb92697041e550e017e62',
    '57f80be82225cdb97c46c494faf36059a65dd04f',
    '6674fccfd9e886a23fe08f07481722869bb62fb1',
    '7088458e1d5c6ccf7dcc6aba967021ce68f24ffb',
    '2b279df8398dd54ccffd6131334eebae01b447db',
    '8242c62be39b7c98cea95f535f294f048e3d5b6f',
    '5e99800d6d278ee9b6b65155dc28ad47863f305e',
    'f7845be0abbbfa191b55fe7018eec00d08f45b2d'
]

def get_qr_as_svg(request, link):
    "genaretae qrcode"

    url = pyqrcode.create(link)
    stream = io.BytesIO()
    url.svg(stream, scale=4)
    return stream.getvalue(), 200, {
        'Content-Type': 'image/svg+xml',
        'Cache-Control': 'no-cache, no-store, must-revalidate',
        'Pragma': 'no-cache',
        'Expires': '0'}


@view_config(route_name='home',
             renderer='h18e:templates/mytemplate.jinja2')
def start(request):
    d = datetime.utcnow()
    n = d + timedelta(days=7)
    week = d.isocalendar()[1]
    id = projekte[week % len(projekte)]
    while d.weekday() != 0:
        d += timedelta(1)
    link = request.route_url("home")
    link = f"{link}project/{id}?kw={week}"
    linkdescription = f"{link}project/{id}?kw={week}"
    val = get_qr_as_svg(request, link)
    val = str(val[0], 'utf-8').replace('class="pyqrcode"','')
    val = val.replace('class="pyqrline"','')[39:]
    link +=f"?kw={week}"
    return {'topic': 'Auswertungen -- global',
            'qrimage': val,
            'link': link,
            'linkdescription': link,
            'week': week,
            'next': n.strftime('%d.%m.%Y')
    }


@view_config(route_name='about',
             renderer='h18e:templates/about.jinja2')
def about(request):
    return {'project': 'about'}

@view_config(route_name='lizenz',
             renderer='h18e:templates/lizenz.jinja2')
def lizenz(request):
    return {'project': 'lizenz'}


@view_config(route_name='project',
             renderer='h18e:templates/project.jinja2')
def project(request):
    d = datetime.utcnow()
    week = d.isocalendar()[1]

    return {'project': request.matchdict['pid'],
            'week': week}
