from pyramid.view import notfound_view_config
from pyramid.view import view_config

@view_config(route_name="robots.txt", renderer='h18e:templates/robots.jinja2')
def robots(request):
    request.response.content_type = 'text/plaub'
    return {}


@notfound_view_config(renderer='h18e:templates/404.jinja2')
def notfound_view(request):
    request.response.status = 404
    return {}
