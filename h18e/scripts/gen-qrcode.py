from datetime import datetime
from random import randint
import qrcode
import qrcode.image.svg
import os.path
import hashlib

def get_qrc(link, organisation="h18e"):
    randomint = randint(0, 1000000)
    randomsom = str(randomint)
    now = datetime.now()
    currenttime = now.strftime(format="%Y-%m-%d %H:%M:%S")
    var = f"{currenttime}_{organisation}_{randomsom}"
    string = var.encode("utf-8")
    hash_object = hashlib.sha1(string).hexdigest()
    factory = qrcode.image.svg.SvgPathImage
    img = qrcode.make(f"{link}", image_factory=factory)
    savepath = f"{link}.svg"
    img.save(savepath)
    return f"{savepath}"

def get_id(organisation="h18e"):
    randomint = randint(0, 1000000)
    randomsom = str(randomint)
    now = datetime.now()
    currenttime = now.strftime(format="%Y-%m-%d %H:%M:%S")
    var = f"{currenttime}_{organisation}_{randomsom}"
    string = var.encode("utf-8")
    hash_object = hashlib.sha1(string).hexdigest()
    return f"{hash_object}"

if __name__ =="__main__":

    id =get_id()
    get_qrc(link=id)

    print(id)
